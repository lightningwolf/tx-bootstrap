#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'ldath'
from twisted.web.template import renderer
from tx_bootstrap.base_elements import BootstrapElement


class ButtonElement(BootstrapElement):
    def __init__(self, config, templates=None):
        template = 'bootstrap:button:button'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config

    @renderer
    def button(self, request, tag):
        css_class = ''
        if 'class' in self._config:
            css_class = self._config['class']

        tag.fillSlots(
            href=self._config['href'],
            icon=self._config['icon'],
            css_class=css_class,
            label=self._config['label'],
        )
        return tag


class ButtonClearElement(BootstrapElement):
    def __init__(self, templates=None):
        template = 'bootstrap:button:clear'
        BootstrapElement.__init__(self, template=template, templates=templates)
