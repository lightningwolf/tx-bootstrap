#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
from twisted.web.template import Element, renderer, XMLString
from tx_bootstrap.templates import bootstrap_templates


class BootstrapElementException(Exception):
    pass


class CoreElement(Element):
    def __init__(self, template, templates=None):
        if not templates:
            templates = {}

        for template_name, template_data in bootstrap_templates.iteritems():
            if template_name not in templates:
                templates[template_name] = template_data

        try:
            loader = templates[template]['loader']
        except KeyError:
            raise BootstrapElementException('No template: {0} for Element'.format(template))

        Element.__init__(self, loader=loader)
        self._empty = XMLString(
            '<t:transparent xmlns:t="http://twistedmatrix.com/ns/twisted.web.template/0.1" />'
        )
        self._templates = templates

    def empty(self):
        return Element(loader=self._empty)

    @staticmethod
    def convert(data, empty='', placeholder='{0}'):
        if isinstance(data, unicode):
            return data.encode('utf8')
        if isinstance(data, datetime):
            return data.strftime('%Y-%m-%d %H:%M')
        if data is None:
            if isinstance(empty, unicode):
                return empty.encode('utf8')
            else:
                return empty
        return str(placeholder.format(data))


class BootstrapElement(CoreElement):
    def __init__(self, template, templates=None):
        CoreElement.__init__(self, template=template, templates=templates)


class HeaderElement(BootstrapElement):
    def __init__(self, title, templates=None, css=None):
        template = 'bootstrap:base:header'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._title = title
        if css:
            self._css = css
        else:
            self._css = []

    @renderer
    def css_files(self, request, tag):
        for css_row in self._css:
            yield tag.clone().fillSlots(css_row=css_row)

    @renderer
    def title(self, request, tag):
        return tag(self._title)


class FooterElement(BootstrapElement):
    def __init__(self, templates=None, js=None):
        template = 'bootstrap:base:footer'
        BootstrapElement.__init__(self, template=template, templates=templates)
        if js:
            self._js = js
        else:
            self._js = []

    @renderer
    def js_files(self, request, tag):
        for js_row in self._js:
            yield tag.clone().fillSlots(js_row=js_row)
