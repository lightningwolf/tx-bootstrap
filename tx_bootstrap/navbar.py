#!/usr/bin/env python
# -*- coding: utf-8 -*-
from twisted.web.template import renderer
from tx_bootstrap.nav import NavElement, ItemElement


class NavBarElement(NavElement):
    def __init__(self, config, path, templates=None):
        template = 'bootstrap:navbar:navbar'
        NavElement.__init__(self, config=config, path=path, template=template, templates=templates)
        self._left_tabs = []
        if 'left_tabs' in config:
            self._left_tabs = self.get_items(config['left_tabs'])
        self._right_tabs = []
        if 'right_tabs' in config:
            self._right_tabs = self.get_items(config['right_tabs'])

    @renderer
    def brand(self, request, tag):
        return tag(self._config['brand'])

    @renderer
    def left_tabs(self, request, tag):
        for item in self._left_tabs:
            yield ItemElement(item=item, templates=self._templates)
        yield self.empty()

    @renderer
    def right_tabs(self, request, tag):
        for item in self._right_tabs:
            yield ItemElement(item=item, templates=self._templates)
        yield self.empty()
