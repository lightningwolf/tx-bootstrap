#!/usr/bin/env python
# -*- coding: utf-8 -*-
from twisted.web.template import renderer
from tx_bootstrap.base_elements import BootstrapElement


class AlertElement(BootstrapElement):
    _titles = {
        'success': 'Well done!',
        'info': 'Heads up!',
        'warning': 'Warning!',
        'danger': 'Oh snap!',
        'error': 'Error!',
        'unknown': 'Unknown!',
        'not_found': 'Not Found!',
    }

    _css_classes = {
        'success': 'alert alert-success alert-dismissable',
        'info': 'alert alert-info alert-dismissable',
        'warning': 'alert alert-warning alert-dismissable',
        'danger': 'alert alert-danger alert-dismissable',
        'error': 'alert alert-danger alert-dismissable',
        'unknown': 'alert alert-danger alert-dismissable',
        'not_found': 'alert alert-warning alert-dismissable',
    }

    def __init__(self, alerts, templates=None):
        template = 'bootstrap:alert:alert'
        BootstrapElement.__init__(self, template=template, templates=templates)
        if isinstance(alerts, dict):
            alerts = [alerts]
        self._alerts = alerts

    @renderer
    def alert_message(self, request, tag):
        for alert in self._alerts:
            if all(k in alert for k in ('m', 't')):
                yield tag.clone().fillSlots(
                    message=self._message(alert),
                    title=self._title(alert),
                    class_name=self._css_class(alert)
                )

    def _message(self, alert):
        return self.convert(alert['m'])

    def _title(self, alert):
        if alert['t'] in self._titles:
            return self._titles[alert['t']]
        else:
            return self._titles['unknown']

    def _css_class(self, alert):
        if alert['t'] in self._css_classes:
            return self._css_classes[alert['t']]
        else:
            return self._css_classes['unknown']
