#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'ldath'
from tx_bootstrap import ConfigurationError


# Default list object actions
default_loa = [
    {
        'key': 'edit',
        'label': 'Edit',
        'icon': 'icon-edit',
        'call': 'set_edit_button'
    },
    {
        'key': 'delete',
        'label': 'Delete',
        'icon': 'icon-trash icon-white',
        'confirm': True,
        'confirm_message': 'Are you sure?',
        'class': 'btn btn-small btn-danger',
        'call': 'set_del_button'
    }
]

# Default back action
default_ba = [
    {
        'key': 'back',
        'label': 'Back to list',
        'url': None,
        'class': 'btn btn-warning',
        'icon': 'icon-backward'
    }
]

# Default create/update submit actions
default_sa = [
    {
        'key': 'save',
        'label': 'Save',
        'class': 'btn btn-primary'
    },
    {
        'key': 'save_and_add',
        'label': 'Save and Add',
        'class': 'btn btn-primary'
    }
]


class ListConfigParser:
    NO_URL = 0
    URL_INTERNAL = 1
    URL_EXTERNAL = 2
    URL_CALL = 3
    HTML = 4

    def __init__(self):
        self.configuration = dict(
            id={},
            display=[],
            actions=[],
            object_actions=[],
            batch={},
            filter={}
        )
        self.filter_data = {}

    def configure(self, configuration):
        if 'display' in configuration.keys():
            self._parse_display(configuration['display'])

        if 'action' in configuration.keys():
            self._parse_action(configuration['action'])

        if 'actions' in configuration.keys():
            self._parse_actions(configuration['actions'])

        if 'object_actions' in configuration.keys():
            self._parse_object_actions(configuration['object_actions'])
        else:
            self._parse_object_actions(default_loa)

        if 'batch' in configuration.keys():
            self._parse_batch(configuration['batch'])

        if 'filter' in configuration.keys():
            self._parse_filter(configuration['filter'])

        self.set_filter_data(self.get_clean_filter_data())

    def is_filter(self):
        return True if len(self.configuration.get('filter', {})) > 0 else False

    def is_actions(self):
        return True if len(self.configuration.get('actions', [])) > 0 else False

    def is_object_actions(self):
        return True if len(self.configuration.get('object_actions', [])) > 0 else False

    def is_batch(self):
        batch = self.configuration.get('batch', {})
        return True if len(batch) > 0 else False

    def get_pk(self):
        return self.configuration.get('pk')

    def get_display(self):
        return self.configuration.get('display', [])

    def get_action(self):
        action = self.configuration.get('action')
        pre = action.copy()
        return pre

    def get_actions(self):
        actions = self.configuration.get('actions', [])
        for action in actions:
            pre = action.copy()
            yield pre

    def get_object_actions(self, call_object=None):
        actions = self.configuration.get('object_actions', [])
        for action in actions:
            pre = action.copy()

            if pre['call'] and call_object is not None:
                pre['type'] = self.URL_CALL
                pre = getattr(call_object, pre['call'])(pre)

            yield pre

    def get_batch(self):
        batch_element = self.configuration.get('batch', {})
        pre = batch_element.copy()
        return pre

    def get_filter(self):
        filter_element = self.configuration.get('filter', {})

        pre = filter_element.copy()
        return pre

    def is_filtered(self):
        clean = self.get_clean_filter_data()
        act = self.get_filter_data()
        if clean == act:
            return False
        return True

    def get_filter_data(self):
        return self.filter_data

    def set_filter_data(self, filter_data):
        self.filter_data = filter_data

    def reset_filter_data(self):
        self.set_filter_data(self.get_clean_filter_data())

    def get_clean_filter_data(self):
        data = {}
        for element in self.configuration['filter']['display']:
            data[element] = None
        return data

    def _parse_display(self, elements):
        for element in elements:
            if not all(k in element for k in ('key', 'label')):
                raise ConfigurationError('Wrong configuration format for list display element')

            if not 'type' in element:
                element['type'] = 'string'

            self.configuration['display'].append(element)

    def _parse_action(self, list_element):
        if not all(k in list_element for k in ('url', )):
            raise ConfigurationError('Wrong configuration format for list element')

        if 'type' not in list_element.keys():
            list_element['type'] = self.NO_URL

        self.configuration['action'] = list_element

    def _parse_actions(self, actions):
        for action in actions:
            parsed = self._parse_action(action)
            self.configuration['actions'].append(parsed)

    def _parse_object_actions(self, actions):
        for action in actions:
            parsed = self._parse_action(action)
            self.configuration['object_actions'].append(parsed)

    def _parse_action(self, action):
        if not all(k in action for k in ('key', 'label')):
            raise ConfigurationError('Wrong configuration format for list actions element')

        if 'type' not in action.keys():
            action['type'] = self.NO_URL

        if 'credential' not in action.keys():
            action['credential'] = None

        if 'confirm' not in action.keys():
            action['confirm'] = False

        if 'confirm_message' not in action.keys():
            action['confirm_message'] = 'Are you sure?'

        if 'class' not in action.keys():
            action['class'] = 'btn btn-small'

        if 'call' not in action.keys():
            action['call'] = False

        if 'visable' not in action.keys():
            action['visable'] = True

        if 'disabled' not in action.keys():
            action['disabled'] = False

        return action

    def _parse_batch(self, batch_element):
        if not all(k in batch_element for k in ('url', )):
            raise ConfigurationError('Wrong configuration format for list filter element')

        if 'type' not in batch_element.keys():
            batch_element['type'] = self.NO_URL

        self.configuration['batch'] = batch_element

    def _parse_filter(self, filter_element):
        if not all(k in filter_element for k in ('session_name', 'display', 'url')):
            raise ConfigurationError('Wrong configuration format for list filter element')

        if 'type' not in filter_element.keys():
            filter_element['type'] = self.NO_URL

        self.configuration['filter'] = filter_element
