#!/usr/bin/env python
# coding=utf8
from twisted.web import resource
from twisted.web.template import flattenString
from tx_bootstrap.base_elements import HeaderElement, FooterElement
from tx_bootstrap.navbar import NavBarElement


class BootstrapResource(resource.Resource):
    def __init__(self):
        resource.Resource.__init__(self)
        self.css = ['/bootstrap/css/main.css']
        self.js = ['/bootstrap/js/main.js']
        self.title = 'Set your page title'

    def start_html(self, request):
        request.write("<!DOCTYPE html>\n")
        request.write("<html lang=\"en\">\n")
        self.generate_header(request=request)

    def generate_header(self, request):
        flattenString(
            None,
            HeaderElement(
                title=self.title,
                css=self.css
            )
        ).addCallback(request.write)
        request.write("\n<body>\n")

    def generate_footer(self, request):
        flattenString(
            None,
            FooterElement(
                js=self.js
            )
        ).addCallback(request.write)

    def finish_html(self, request):
        self.generate_footer(request=request)
        request.write("\n</body>\n")
        request.write("</html>")
        request.finish()

    @staticmethod
    def generate_nav_bar(request, config):
        flattenString(
            request=request,
            root=NavBarElement(
                config=config,
                path=request.prepath[0]
            )
        ).addCallback(request.write)
