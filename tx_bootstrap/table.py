#!/usr/bin/env python
# -*- coding: utf-8 -*-
from twisted.web.template import renderer
from tx_bootstrap.base_elements import BootstrapElement
from tx_bootstrap.button import ButtonElement


class TableElement(BootstrapElement):
    def __init__(self, config, data, templates=None):
        template = 'bootstrap:table:table'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config
        self._data = data

    @renderer
    def table(self, request, tag):
        css_class = 'table table-condensed table-bordered table-striped'
        if 'css_class' in self._config:
            css_class = self._config['css_class']

        tag.fillSlots(
            css_class=css_class,
        )
        return tag

    @renderer
    def head(self, request, tag):
        return TableHeadElement(
            config=self._config,
            templates=self._templates
        )

    @renderer
    def body(self, request, tag):
        for data in self._data:
            yield TableBodyElement(
                config=self._config,
                data=data,
                templates=self._templates
            )


class TableHeadElement(BootstrapElement):
    def __init__(self, config, templates=None):
        template = 'bootstrap:table:thead'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config

    @renderer
    def batch(self, request, tag):
        batch = False
        if 'batch' in self._config:
            batch = self._config['batch']
        if batch:
            return TableThBatchElement(
                config=self._config,
                templates=self._templates
            )
        else:
            return self.empty()

    @renderer
    def rows(self, request, tag):
        for element in self._config['display']:
            yield TableThElement(
                config=element,
                templates=self._templates
            )

    @renderer
    def actions(self, request, tag):
        actions = None
        if 'actions' in self._config:
            actions = self._config['actions']
        if actions:
            return TableThElement(
                config=actions,
                templates=self._templates
            )
        else:
            return self.empty()


class TableThElement(BootstrapElement):
    def __init__(self, config, templates=None):
        template = 'bootstrap:table:th'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config

    @renderer
    def th(self, request, tag):
        css_class = ''
        if 'class' in self._config:
            css_class = self._config['class']

        tag.fillSlots(
            key=self._config['key'],
            label=self._config['label'],
            css_class=css_class
        )
        return tag


class TableThBatchElement(BootstrapElement):
    def __init__(self, config, templates=None):
        template = 'bootstrap:table:th_batch'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config


class TableBodyElement(BootstrapElement):
    def __init__(self, config, data, templates=None):
        template = 'bootstrap:table:tbody'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config
        self._data = data

    @renderer
    def batch(self, request, tag):
        batch = False
        if 'batch' in self._config:
            batch = self._config['batch']
        if batch:
            return TableTdBatchElement(
                config=self._config,
                templates=self._templates
            )
        else:
            return self.empty()

    @renderer
    def rows(self, request, tag):
        for element in self._config['display']:
            yield TableTdElement(
                config=element,
                data=self._data[element['key']],
                templates=self._templates
            )

    @renderer
    def actions(self, request, tag):
        actions = None
        if 'object_actions' in self._config:
            actions = self._config['object_actions']
        if actions:
            return TableTdActionsElement(
                config=actions,
                data=self._data,
                templates=self._templates
            )
        else:
            return self.empty()


class TableTdElement(BootstrapElement):
    def __init__(self, config, data, templates=None):
        template = 'bootstrap:table:td'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config
        self._data = data

    @renderer
    def td(self, request, tag):
        css_class = ''
        if 'class' in self._config:
            css_class = self._config['class']

        tag.fillSlots(
            key=self._config['key'],
            value=self.convert(self._data, empty=self._config['empty']),
            css_class=css_class,
        )
        return tag


class TableTdBatchElement(BootstrapElement):
    def __init__(self, config, templates=None):
        template = 'bootstrap:table:td_batch'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config


class TableTdActionsElement(BootstrapElement):
    def __init__(self, config, data, templates=None):
        template = 'bootstrap:table:td_actions'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config
        self._data = data

    @renderer
    def actions(self, request, tag):
        for action in self._config:
            formats = []
            for k in action['url_format']:
                formats.append(self._data[k])
            action['href'] = action['url_string'].format(*formats)
            yield ButtonElement(
                config=action,
                templates=self._templates
            )
