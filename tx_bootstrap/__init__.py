#!/usr/bin/env python
# coding=utf8
__author__ = 'Arkadiusz Tulodziecki'
__email__ = 'atulodzi@gmail.com'
__version__ = '0.1.5'


class ConfigurationError(RuntimeError):
    pass
