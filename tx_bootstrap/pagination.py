# !/usr/bin/env python
# -*- coding: utf-8 -*-
from twisted.web.template import renderer
from tx_bootstrap.base_elements import BootstrapElement


class PaginationElement(BootstrapElement):
    def __init__(self, pager, templates=None):
        template = 'bootstrap:pagination:pagination'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._pager = pager

    @renderer
    def links(self, request, tag):
        for page in self._pager.get_links():
            li_class = ''
            if page == self._pager.get_page():
                li_class = 'active'
            yield tag.clone().fillSlots(
                li_class=li_class,
                link_href='?page={0}'.format(page),
                link_name=self.convert(page)
            )

    @renderer
    def first(self, request, tag):
        li_class = ''
        if self._pager.get_page() == 1:
            li_class = 'disabled'
        tag.fillSlots(
            li_class=li_class,
        )
        return tag

    @renderer
    def last(self, request, tag):
        li_class = ''
        if self._pager.get_last_page() == self._pager.get_page():
            li_class = 'disabled'
        tag.fillSlots(
            li_class=li_class,
            link_href='?page={0}'.format(self._pager.get_last_page()),
        )
        return tag
