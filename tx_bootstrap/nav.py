#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from twisted.web.template import renderer
from tx_bootstrap.base_elements import BootstrapElement


class NavElement(BootstrapElement):
    def __init__(self, config, path,  template, templates=None):
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._config = config
        self._path = path

    def get_items(self, tabs):
        if tabs:
            if 'children' in tabs:
                return self.parse_items(tabs['children'])
        return []

    def parse_items(self, pre_items):
        items = []
        for item in pre_items:
            if re.match('^%s$' % item['match'], self._path):
                view_class = 'active'
            else:
                view_class = ''

            url = item['url']
            items.append(
                {'view_class': view_class, 'url': url, 'label': item['label']}
            )
        return items


class ItemElement(BootstrapElement):
    def __init__(self, item,  templates=None):
        template = 'bootstrap:nav:item'
        BootstrapElement.__init__(self, template=template, templates=templates)
        self._item = item

    @renderer
    def item(self, request, tag):
        tag.fillSlots(
            class_name=self._item['view_class'],
            url=self._item['url'],
            label=self._item['label'],
        )
        return tag


class PillsElement(NavElement):
    def __init__(self, config, path, templates=None):
        template = 'bootstrap:nav:pills'
        NavElement.__init__(self, config=config, path=path, template=template, templates=templates)

    @renderer
    def tabs(self, request, tag):
        items = self.parse_items(self._config['items'])
        for item in items:
            yield ItemElement(item=item, templates=self._templates)
