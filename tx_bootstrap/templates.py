# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'ldath'

import os

from twisted.web.template import XMLFile
from twisted.python.filepath import FilePath

module_root = os.path.dirname(os.path.realpath(__file__))
bootstrap_templates_root = '{0}/templates'.format(module_root)
bootstrap_static_root = '{0}/static'.format(module_root)

bootstrap_templates = {
    'bootstrap:base:footer': {
        'loader': XMLFile(FilePath('{0}/footer.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:base:header': {
        'loader': XMLFile(FilePath('{0}/header.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:base:sign_in': {
        'loader': XMLFile(FilePath('{0}/sign_in.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:alert:alert': {
        'loader': XMLFile(FilePath('{0}/alert/alert.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:nav:item': {
        'loader': XMLFile(FilePath('{0}/nav/item.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:nav:pills': {
        'loader': XMLFile(FilePath('{0}/nav/pills.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:navbar:navbar': {
        'loader': XMLFile(FilePath('{0}/navbar/navbar.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:table:table': {
        'loader': XMLFile(FilePath('{0}/table/table.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:table:thead': {
        'loader': XMLFile(FilePath('{0}/table/thead.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:table:tbody': {
        'loader': XMLFile(FilePath('{0}/table/tbody.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:table:th': {
        'loader': XMLFile(FilePath('{0}/table/th.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:table:th_batch': {
        'loader': XMLFile(FilePath('{0}/table/th_batch.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:table:td': {
        'loader': XMLFile(FilePath('{0}/table/td.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:table:td_batch': {
        'loader': XMLFile(FilePath('{0}/table/td_batch.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:table:td_actions': {
        'loader': XMLFile(FilePath('{0}/table/td_actions.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:pagination:pagination': {
        'loader': XMLFile(FilePath('{0}/pagination/pagination.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:button:button': {
        'loader': XMLFile(FilePath('{0}/button/button.xml'.format(bootstrap_templates_root)))
    },
    'bootstrap:button:clear': {
        'loader': XMLFile(FilePath('{0}/button/clear.xml'.format(bootstrap_templates_root)))
    },
}
