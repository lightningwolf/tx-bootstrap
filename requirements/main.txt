# Setup and distribute
wheel>=0.23.0
pip>=1.5.5

# Twisted
Twisted>=14.0.0,<15.0.0
