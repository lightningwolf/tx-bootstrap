#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

try:
    from setuptools import setup, Command, find_packages
except ImportError:
    from distutils.core import setup, Command
    from findpackages import find_packages

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

from pip.req import parse_requirements

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()


def parse_requirements_file(path):
    # parse_requirements() returns generator of pip.req.InstallRequirement objects
    requirements = parse_requirements(path)
    return [str(ir.req) for ir in requirements]


def get_console_scripts(package_name):
    console_scripts = []
    for root, _, files in os.walk("{0}/scripts/".format(package_name)):
        for file_name in files:
            base_name, ext = os.path.splitext(file_name)
            if ext != ".py":
                continue
            module = ".".join(os.path.join(root, base_name).split(os.path.sep))
            console_script = "{0} = {1}:main".format(base_name, module)
            console_scripts.append(console_script)
    return console_scripts


def main():
    from tx_bootstrap import __version__
    readme = open('README.rst').read()
    history = open('HISTORY.rst').read().replace('.. :changelog:', '')
    install_requires = parse_requirements_file('requirements.txt')
    tests_require = parse_requirements_file('requirements/test.txt')
    packages = find_packages()
    console_scripts = get_console_scripts('tx_bootstrap')

    setup(
        name='tx-bootstrap',
        version=__version__,
        description='Bootstrap templates for Twisted Template system.',
        long_description=readme + '\n\n' + history,
        author='Arkadiusz Tulodziecki',
        author_email='atulodzi@gmail.com',
        url='https://github.com/ldath/tx-bootstrap',
        packages=packages,
        include_package_data=True,
        install_requires=install_requires,
        extras_require={
            'tests': tests_require,
        },
        tests_require=tests_require,
        entry_points={
            'console_scripts': console_scripts,
        },
        license="MIT",
        zip_safe=False,
        keywords='tx_bootstrap',
        classifiers=[
            'Development Status :: 1 - Planning',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: MIT License',
            'Natural Language :: English',
            "Programming Language :: Python :: 2",
            'Programming Language :: Python :: 2.7',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.3',
        ],
        test_suite='tests',
    )

if __name__ == '__main__':
    main()