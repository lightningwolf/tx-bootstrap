.. :changelog:

History
-------

0.1.5 (2014-06-30)
++++++++++++++++++

- Bug fix in table th element

0.1.4 (2014-06-16)
++++++++++++++++++

- Table elements in first working version

0.1.3 (2014-06-04)
++++++++++++++++++

- New list config parser class (origin Flask-LwAdmin)
- New pager class (origin Flask-LwAdmin)

0.1.2 (2014-05-26)
++++++++++++++++++

- Bug fix in alert.

0.1.0 (2014-05-25)
++++++++++++++++++

- First development release on PyPI.