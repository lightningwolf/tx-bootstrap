===========================
Twisted Bootstrap templates
===========================


Bootstrap templates for Twisted Template system
-----------------------------------------------

- Free software: MIT license

tx-bootstrap is a set of xml Twisted templates, classes, and methods to assist in the creation of administrative panels
based on the twisted framework. At the moment we are working on the main section of the navigation and basic helpers
for CRUD model.


Versioning
++++++++++

The project uses the `Semantic Versioning 2.0.0 <http://semver.org/>`_. Work is currently underway,
but because Flask-LwAdmin is already being used in production enviroment then API changes occur only in case of change parameter **y** in (**0.y.z**)

Examples
++++++++

Check SAMPLE_PROJECT for basic use example. More advanced use in this moment can be check in:
`Example Twisted Project <https://git.thunderwolf.net/examples/ex-twisted>`_

Documentation
+++++++++++++

The documentation will be created in the near future. We know how important good documentation is for the proper development of the project.

How you can help
----------------

Comments
++++++++

We like comments.


Bug reports
+++++++++++

If you want to help us out, please file a bug report on our tracker at:

  `Gitlab tx-bootstrap Issues <https://git.thunderwolf.net/lightningwolf/tx-bootstrap/issues>`_

Please be as descriptive as possible and tell us what exactly you were doing
at the time something went wrong. If possible, send us a exception backtrace and if possible
example code.


Wishes
++++++

We can't possibly know what everyone wants, so we appreciate all feature
requests. These can be submitted to the issue tracker as well (see above).

**Any opinion and ideas are welcome.**


Programming
+++++++++++

Patches are welcome.
