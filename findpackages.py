#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os


def is_package(path):
    return (
        os.path.isdir(path) and
        os.path.isfile(os.path.join(path, '__init__.py'))
    )


def find_packages(path=".", base=""):
    """ Find all packages in path """
    packages = {}
    for item in os.listdir(path):
        directory = os.path.join(path, item)
        if is_package(directory):
            if base:
                module_name = "%(base)s.%(item)s" % vars()
            else:
                module_name = item
            packages[module_name] = directory
            packages.update(find_packages(directory, module_name))
    return packages