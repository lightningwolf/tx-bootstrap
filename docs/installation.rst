============
Installation
============

At the command line::

    $ easy_install tx_bootstrap

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv tx_bootstrap
    $ pip install tx_bootstrap